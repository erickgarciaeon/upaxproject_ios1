//
//  Connection.swift
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class Connection {
    static func get(url: String?, headers: HTTPHeaders, closure: @escaping (GenericResponse?, Error?) -> Void) {
        print("""
            =======================================================================================================
            
            REQUEST INFO
            
            HttpMethod:     Get
            Url:            \(url!)
            Headers:        \(String(describing: headers))
            """)
        AF.request(url ?? "", method: .get, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<500)
            .responseJSON {
                (response) in
                print("\n       RESPONSE INFO:: \(String(describing: response.result))")
                switch response.result {
                case .success(let value):
                    print("=================== \n\n SUCCESS: \(String(describing: value))")
                    let code = response.response?.statusCode
                    let generic = GenericResponse(code: code!, response: value as! [String: Any])
                    closure(generic, nil)
                    break
                case .failure(let error):
                    print("=================== \n\n Error: \(String(describing: error))")
                    closure(nil, error)
                    break
                }
        }
    }

    static func post(url: String?, param: Parameters, headers: HTTPHeaders, closure: @escaping (GenericResponse?, Error?) -> Void) {
        print("""
            =======================================================================================================
            
            REQUEST INFO
            
            HttpMethod:     Post
            Url:            \(url!)
            Headers:        \(String(describing: headers))
            Body:           \(String(describing: param))
            """)
        AF.request(url ?? "", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<500)
            .responseJSON {
                (response) in
                print("\n       RESPONSE INFO:: \(String(describing: response.result))")
                switch response.result {
                case .success(let value):
                    print("=================== \n\n SUCCESS: \(String(describing: value))")
                    let code = response.response?.statusCode
                    let generic = GenericResponse(code: code!, response: value as! [String: Any])
                    closure(generic, nil)
                    break
                case .failure(let error):
                    print("=================== \n\n Error: \(String(describing: error))")
                    closure(nil, error)
                    break
                }
        }
    }

    static func put(url: String?, param: Parameters, headers: HTTPHeaders, closure: @escaping (GenericResponse?, Error?) -> Void) {
        AF.request(url ?? "", method: .put, parameters: param, encoding: JSONEncoding.default, headers: headers)
            .validate(statusCode: 200..<500)
            .responseJSON {
                (response) in
                switch response.result {
                case .success(let value):
                    let code = response.response?.statusCode
                    let generic = GenericResponse(code: code!, response: value as! [String: Any])
                    closure(generic, nil)
                    break
                case .failure(let error):
                    closure(nil, error)
                    break
                }
        }
    }
}

