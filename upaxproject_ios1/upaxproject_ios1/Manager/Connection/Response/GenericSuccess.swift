//
//  GenericSuccess.swift
//

import Foundation
import ObjectMapper

private let CODE = "code"
private let DATA = "data"

class GenericSuccess: Mappable {
    internal var code: Int?
    internal var data: String? = ""
    
    required init?(map:Map) {
        mapping(map: map)
    }
    
    func mapping(map:Map) {
        code <- map[CODE]
        data <- map[DATA]
    }
}
