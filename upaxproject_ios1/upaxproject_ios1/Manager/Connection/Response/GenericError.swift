//
//  GenericError.swift
//

import Foundation
import ObjectMapper

private let TITLE = "title"
private let MESSAGE = "message"
private let CODE = "code"


class GenericError: Mappable {
    internal var title: String?
    internal var message: String?
    internal var code: String?
    
    
    required init?(map:Map)
    {
        mapping(map: map)
    }
    
    
    func mapping(map:Map)
    {
        title <- map[TITLE]
        message <- map[MESSAGE]
        code <- map[CODE]
    }
}
