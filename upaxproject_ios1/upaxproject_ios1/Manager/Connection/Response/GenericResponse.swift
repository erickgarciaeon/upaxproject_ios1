//
//  GenericResponse.swift
//

import Foundation

class GenericResponse {
    var code: Int = Int()
    var response: [String: Any]
    
    
    init(code: Int,response: [String: Any]) {
        self.code = code
        self.response = response
    }
}
