//
//  CasinoServices.swift
//

import Foundation
import ObjectMapper
import Alamofire

class TableUpaxServices {
    static func tableServiceRequest(completion: @escaping (GenericResponse?, Error?) -> ()) {
        let url = Utils.CreateURL(requestName: Constants.Endpoints.tableService).urlString
        let parameters: Parameters = [:]
        let headers: HTTPHeaders = [Constants.HeaderKeys.contentType: Constants.HeaderValues.jsonValue]
        Connection.post(url: url, param: parameters, headers: headers) { (response, error) in
            if error != nil
            {
                completion(nil, error)
            }
            else
            {
                completion(response, nil)
            }
        }
    }
}
