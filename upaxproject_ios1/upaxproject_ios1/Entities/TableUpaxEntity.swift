//
//  HomeEntity.swift
//

import Foundation
import ObjectMapper

class DataModel: Mappable {
    var colors: [String]?
    var questions: [Questions]?
    
    public convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        colors <- map["colors"]
        questions <- map["questions"]
    }
}

class Questions: Mappable {
    var total: String?
    var text: String?
    var chartData: [Chart]?
    
    public convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        text <- map["text"]
        chartData <- map["chartData"]
    }
}

class Chart: Mappable {
    var text: String?
    var percetnage: String?
    
    public convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        text <- map["text"]
        percetnage <- map["percetnage"]
    }
}
