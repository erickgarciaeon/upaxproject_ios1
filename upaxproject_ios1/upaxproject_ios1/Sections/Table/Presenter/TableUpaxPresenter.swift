//
//  TableUpaxPresenter.swift
//

import UIKit

class TableUpaxPresenter: TableUpaxOutput {
    weak var view: TableUpaxView?
    var provider: TableUpaxProvider?
    var wireframe: TableUpaxWireFrame?

    func dataResponse<T>(response: T) {
        switch response {
        case is DataModel:
            view?.dataResponseSuccess(response: response as! DataModel)
        case is String:
            view?.dataResponseServiceFail(response: response as! String)
        default:
            break
        }
    }
    
}

extension TableUpaxPresenter: TableUpaxEventHandler {
    func sendDataRequest() {
        provider?.getData()
    }
}
