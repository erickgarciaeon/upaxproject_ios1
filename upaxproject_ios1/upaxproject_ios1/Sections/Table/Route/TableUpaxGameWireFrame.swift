//
//  TableUpaxWireFame.swift
//

import UIKit

class TableUpaxWireFrame {
    private var view: TableUpaxViewController?
    private let presenter: TableUpaxPresenter?
    private let interactor: TableUpaxInteractor?
    private var window: UIWindow?

    init(in window: UIWindow?) {
        self.view = TableUpaxViewController()
        self.presenter = TableUpaxPresenter()
        self.interactor = TableUpaxInteractor()

        self.view?.eventHandler = self.presenter
        self.presenter?.view = self.view
        self.presenter?.provider = self.interactor
        self.interactor?.output = presenter
        self.presenter?.wireframe = self
        self.window = window
        self.view?.windowBase = window
    }

    func pushTableUpaxViewController() {
        let navigation = window?.rootViewController as! UINavigationController
        view?.modalPresentationStyle = .overCurrentContext
        navigation.pushViewController(view!, animated: true)
    }

    func rootTableUpaxViewController() {
        self.window?.rootViewController = UINavigationController(rootViewController: self.view!)
        self.window?.makeKeyAndVisible()
    }
}
