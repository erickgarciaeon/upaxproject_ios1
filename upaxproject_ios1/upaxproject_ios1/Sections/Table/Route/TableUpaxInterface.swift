//
//  TableUpaxInterface.swift
//

import UIKit

protocol TableUpaxView: class {
    func dataResponseSuccess(response: DataModel)
    func dataResponseServiceFail(response: String)
}

protocol TableUpaxEventHandler {
    func sendDataRequest()
}

protocol TableUpaxOutput: class {
    func dataResponse<T>(response: T)
}

protocol TableUpaxProvider {
    func getData()
}
