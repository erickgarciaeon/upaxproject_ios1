//
//  TableUpaxInteractor.swift
//

class TableUpaxInteractor: TableUpaxProvider {
    weak var output: TableUpaxOutput?

    func getData() {
        TableUpaxServices.tableServiceRequest(){
            (response, error) in
            if error != nil {
                self.output?.dataResponse(response: error?.localizedDescription)
            } else {
                let responseModel = DataModel(JSON: (response?.response)!)
                if responseModel != nil {
                    self.output?.dataResponse(response: responseModel)
                } else {
                    self.output?.dataResponse(response: "Error")
                }            }
        }
    }
    
}
