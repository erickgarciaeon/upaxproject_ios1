//
//  TableUpaxViewController.swift
//

import UIKit

class TableUpaxViewController: UIViewController {
    var eventHandler: TableUpaxEventHandler?
    var windowBase: UIWindow?
    
    private var tableUpaxViewCollection: UICollectionView!
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.view.backgroundColor = UIColor.returnRGBColor(r: 29, g: 36, b: 46, alpha: 1)
        
        createView()
        addViews()
        setupLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //view.backgroundColor = .red
        eventHandler?.sendDataRequest()
        tableUpaxViewCollection.reloadData()
    }
}


extension TableUpaxViewController {
    func createView(){
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: self.view.frame.width, height: 100)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 15
        
        tableUpaxViewCollection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        tableUpaxViewCollection?.register(TableUpaxOneViewCell.self, forCellWithReuseIdentifier: TableUpaxOneViewCell.reuseIdentifier)
        tableUpaxViewCollection.delegate = self
        tableUpaxViewCollection.dataSource = self
        tableUpaxViewCollection.backgroundColor = .clear
        tableUpaxViewCollection.contentInset.bottom = 100
        tableUpaxViewCollection.showsVerticalScrollIndicator = false
        tableUpaxViewCollection.showsHorizontalScrollIndicator = false
    }
    
    func addViews(){
        self.view.addSubview(tableUpaxViewCollection)
    }
    
    func setupLayout(){
        tableUpaxViewCollection.topAnchor(equalTo: self.view.safeTopAnchor, constant: 20)
        tableUpaxViewCollection.heightAnchor(equalTo: 40)
        tableUpaxViewCollection.leadingAnchor(equalTo: self.view.leadingAnchor, constant: 20)
        tableUpaxViewCollection.trailingAnchor(equalTo: self.view.trailingAnchor, constant: -20)
        tableUpaxViewCollection.bottomAnchor(equalTo: self.view.safeBottomAnchor, constant: 20 - 64)
    }
}

extension TableUpaxViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TableUpaxOneViewCell.reuseIdentifier, for: indexPath) as! TableUpaxOneViewCell
                
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //[indexPath.row]
    }
}

extension TableUpaxViewController: TableUpaxView {
    func dataResponseSuccess(response: DataModel) {
        //do something with response
    }

    func dataResponseServiceFail(response: String) {
        Utils.showAlert(title: "Error", message: "ocurrio un problema al consumir el servicio.", context: self)
    }
}
