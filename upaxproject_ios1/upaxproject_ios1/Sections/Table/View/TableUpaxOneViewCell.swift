//
//  TableUpaxViewCell.swift
//

import UIKit

class TableUpaxOneViewCell: UICollectionViewCell {
    // MARK: - Reuse Identifier
    static let reuseIdentifier = "\(TableUpaxOneViewCell.self)"

    // MARK: - View Properties
    private let tableUpaxTextField = UITextField()
    
    // MARK: - View Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayouts()
        self.backgroundColor = .yellow
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        tableUpaxTextField.text = "Nombre"
    }

}

extension TableUpaxOneViewCell {
    private func setupLayouts() {
        configureTableUpaxTextField()
    }
    

    private func configureTableUpaxTextField() {
        tableUpaxTextField.font = UIFont(name: Constants.Fonts.regular, size: 14)
        tableUpaxTextField.textColor = UIColor.returnRGBColor(r: 207, g: 212, b: 222, alpha: 1.0)
        addSubview(tableUpaxTextField)
        
        tableUpaxTextField.topAnchor(equalTo: self.topAnchor, constant: 5)
        tableUpaxTextField.heightAnchor(equalTo: 20.0)
        tableUpaxTextField.leadingAnchor(equalTo: leadingAnchor)
        tableUpaxTextField.trailingAnchor(equalTo: trailingAnchor)
    }

}
