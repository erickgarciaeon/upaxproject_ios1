//
//  Sesion.swift
//

import Foundation
import UIKit

final class Session {
    static let sharedInstance = Session()
    
    private init() {
        // Intentionally unimplemented...
    }
    
    var currentWindow: UIWindow?
}
