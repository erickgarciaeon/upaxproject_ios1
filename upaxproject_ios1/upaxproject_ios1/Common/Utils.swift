//
//  Utils.swift
//

import Foundation
import UIKit

class Utils: NSObject {

    struct ServerConfig {
        static let url = "https://us-central1-bibliotecadecontenido.cloudfunctions.net/"
    }

    struct CreateURL {
        var urlString = ""

        init(requestName: String) {
            urlString = String.init(format: "%@%@", ServerConfig.url, requestName)
        }
    }

    static func showAlert(title: String, message: String, context: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style {
            case .default:
                print("default")
                break
            case .cancel:
                print("cancel")
                break
            case .destructive:
                print("destructive")
                break

            @unknown default:
                break
            } }))
        context.present(alert, animated: true, completion: nil)
    }
}
