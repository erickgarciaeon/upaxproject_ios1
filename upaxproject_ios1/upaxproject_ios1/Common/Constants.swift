//
//  Constants.swift
//

import Foundation
import UIKit


struct Constants {

    struct Endpoints {
        static let tableService = "helloWorld"
    }
    
    struct HeaderKeys {
        static let contentType = "Content-Type"
        static let authorization = "Authorization"
    }

    struct HeaderValues {
        static let jsonValue = "application/json"
    }
    
    struct Fonts {
        static let italic = "HelveticaNeue-Italic"
        static let bold = "HelveticaNeue-Bold"
        static let lightItalic = "HelveticaNeue-LightItalic"
        static let light = "HelveticaNeue-Light"
        static let mediumItalic = "HelveticaNeue-MediumItalic"
        static let medium = "HelveticaNeue-Medium"
        static let regular = "HelveticaNeue"
    }

    static let chartDescription = "Una gráfica o representación gráfica es un tipo de representación de datos, generalmente numéricos, mediante recursos visuales (líneas, vectores, superficies o símbolos), para que se manifieste visualmente la relación matemática o correlación estadística que guardan entre sí. También es el nombre de un conjunto de puntos que se plasman en coordenadas cartesianas y sirven para analizar el comportamiento de un proceso o un conjunto de elementos o signos que permiten la interpretación de un fenómeno. La representación gráfica permite establecer valores que no se han obtenido experimentalmente sino mediante la interpolación (lectura entre puntos) y la extrapolación (valores fuera del intervalo experimental)."
}
