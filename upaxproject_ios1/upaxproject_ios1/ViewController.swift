//
//  ViewController.swift
//  upaxproject_ios1
//
//  Created by ICOM on 05/07/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Entry point for Viper
        let frame = TableUpaxWireFrame(in: Session.sharedInstance.currentWindow)
        frame.rootTableUpaxViewController()
    }


}

