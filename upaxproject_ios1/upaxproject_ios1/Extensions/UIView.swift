//
//  UIView.swift
//

import Foundation
import UIKit

extension UIView {
    
    var id: String? {
        get {
            return self.accessibilityIdentifier
        }
        set {
            self.accessibilityIdentifier = newValue
        }
    }
    
    func view(withId id: String) -> UIView? {
        if self.id == id {
            return self
        }
        for view in self.subviews {
            if let view = view.view(withId: id) {
                return view
            }
        }
        return nil
    }
    
    public var viewController: UIViewController? {
        var parent: UIResponder? = self
        while parent != nil {
            parent = parent?.next
            if let viewController = parent as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    
    private func addShadow(offset: CGSize, color: UIColor, opacity: Float, radius: CGFloat) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
    func addGradient(topColor: UIColor, bottomColor: UIColor) {
        //Background Gradient
        let gradient = CAGradientLayer()
        gradient.colors = [topColor.cgColor, bottomColor.cgColor]
        gradient.locations = [0.0, 1.0]
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
            
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    public func removeAllConstraints() {
        var _superview = self.superview
        
        while let superview = _superview {
            for constraint in superview.constraints {
                
                if let first = constraint.firstItem as? UIView, first == self {
                    superview.removeConstraint(constraint)
                }
                
                if let second = constraint.secondItem as? UIView, second == self {
                    superview.removeConstraint(constraint)
                }
            }
            
            _superview = superview.superview
        }
        
        self.removeConstraints(self.constraints)
        self.translatesAutoresizingMaskIntoConstraints = true
    }
    
    // MARK: - Safe anchors
    
    /// Contains view's top anchor depending to iOS version.
    var safeTopAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.topAnchor
        } else {
            return self.topAnchor
        }
    }
    
    /// Contains view's leading anchor depending to iOS version.
    var safeLeadingAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.leadingAnchor
        } else {
            return self.leadingAnchor
        }
    }
    
    /// Contains view's trailing anchor depending to iOS version.
    var safeTrailingAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.trailingAnchor
        } else {
            return self.trailingAnchor
        }
    }
    
    /// Contains view's bottom anchor depending to iOS version.
    var safeBottomAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.bottomAnchor
        } else {
            return self.bottomAnchor
        }
    }
    
    // MARK: - Common constraints
    
    /// Adds a constraint (top) that defines one item’s attribute as equal to another item’s attribute plus a constant offset.
    /// - Parameters:
    ///   - anchor: A layout anchor.
    ///   - constant: The constant offset for the constraint.
    @discardableResult
    func topAnchor(equalTo anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    
    /// Adds a constraint (bottom) that defines one item’s attribute as equal to another item’s attribute plus a constant offset.
    /// - Parameters:
    ///   - anchor: A layout anchor.
    ///   - constant: The constant offset for the constraint.
    @discardableResult
    func bottomAnchor(equalTo anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        bottomAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    
    /// Adds a constraint (leading) that defines one item’s attribute as equal to another item’s attribute plus a constant offset.
    /// - Parameters:
    ///   - anchor: A layout anchor.
    ///   - constant: The constant offset for the constraint.
    @discardableResult
    func leadingAnchor(equalTo anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        leadingAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    
    /// Adds a constraint (trailing) that defines one item’s attribute as equal to another item’s attribute plus a constant offset.
    /// - Parameters:
    ///   - anchor: A layout anchor.
    ///   - constant: The constant offset for the constraint.
    @discardableResult
    func trailingAnchor(equalTo anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        trailingAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    
    // MARK: - Height constraints
    
    /// Adds a height constraint with constant value.
    /// - Parameter constant: The constant offset for the constraint.
    @discardableResult
    func heightAnchor(equalTo constant: CGFloat) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: constant).isActive = true
        return self
    }
    
    /// Adds a constraint (height) that defines one item’s attribute as equal to another item’s attribute plus a constant offset.
    /// - Parameters:
    ///   - anchor: A layout anchor.
    ///   - multiplier: The multiplier constant for the constraint.
    ///   - constant: The constant offset for the constraint.
    @discardableResult
    func heightAnchor(equalTo anchor: NSLayoutDimension, multiplier: CGFloat = 1, constant: CGFloat = 0) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalTo: anchor, multiplier: multiplier, constant: constant).isActive = true
        return self
    }
    
    // MARK: - Width constraints
    
    /// Adds a width constraint with constant value.
    /// - Parameter constant: The constant offset for the constraint.
    @discardableResult
    func widthAnchor(equalTo constant: CGFloat) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: constant).isActive = true
        return self
    }
    
    /// Adds a constraint (width) that defines one item’s attribute as greater than or equal to another item’s attribute plus a constant offset.
    /// - Parameter constant: The constant offset for the constraint.
    @discardableResult
    func widthAnchor(greaterThanOrEqualTo constant: CGFloat) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(greaterThanOrEqualToConstant: constant).isActive = true
        return self
    }
    
    /// Adds a constraint (width) that defines one item’s attribute as less than or equal to another item’s attribute plus a constant offset.
    /// - Parameter constant: The constant offset for the constraint.
    @discardableResult
    func widthAnchor(lessThanOrEqualToConstant constant: CGFloat) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(lessThanOrEqualToConstant: constant).isActive = true
        return self
    }
    
    /// Adds a constraint (width) that defines one item’s attribute as equal to another item’s attribute plus a constant offset.
    /// - Parameters:
    ///   - anchor: A layout anchor.
    ///   - constant: The constant offset for the constraint.
    @discardableResult
    func widthAnchor(equalTo anchor: NSLayoutDimension, constant: CGFloat = 0) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    
    // MARK: - Center constraints
    
    /// Adds a constraint (center X) that defines one item’s attribute as equal to another item’s attribute plus a constant offset.
    /// - Parameters:
    ///   - anchor: A layout anchor.
    ///   - constant: The constant offset for the constraint.
    @discardableResult
    func centerXAnchor(equalTo anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    
    /// Adds a constraint (center Y) that defines one item’s attribute as equal to another item’s attribute plus a constant offset.
    /// - Parameters:
    ///   - anchor: A layout anchor.
    ///   - constant: The constant offset for the constraint.
    @discardableResult
    func centerYAnchor(equalTo anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        centerYAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    
    
    // MARK: - Helpers
    
    /// Adds four constraints (top, bottom, leading, trailing) to the current view and another view.
    /// - Parameters:
    ///   - anotherView: The view for the right side of the constraint.
    ///   - insets: The constant inset for the constraint.
    ///   - usingSafeArea: Use safe area or standart anchors.
    func pinAnchors(to anotherView: UIView, insets: UIEdgeInsets = .zero, usingSafeArea: Bool = false) {
        if usingSafeArea {
            self
                .topAnchor(equalTo: anotherView.safeTopAnchor, constant: insets.top)
                .leadingAnchor(equalTo: anotherView.safeLeadingAnchor, constant: insets.left)
                .trailingAnchor(equalTo: anotherView.safeTrailingAnchor, constant: -insets.right)
                .bottomAnchor(equalTo: anotherView.safeBottomAnchor, constant: -insets.bottom)
        } else {
            self
                .topAnchor(equalTo: anotherView.topAnchor, constant: insets.top)
                .leadingAnchor(equalTo: anotherView.leadingAnchor, constant: insets.left)
                .trailingAnchor(equalTo: anotherView.trailingAnchor, constant: -insets.right)
                .bottomAnchor(equalTo: anotherView.bottomAnchor, constant: -insets.bottom)
        }
    }
}

extension NSObject {
    func setAssociatedObject(_ value: AnyObject?, associativeKey: UnsafeRawPointer, policy: objc_AssociationPolicy) {
        if let valueAsAnyObject = value {
            objc_setAssociatedObject(self, associativeKey, valueAsAnyObject, policy)
        }
    }
    
    func getAssociatedObject(_ associativeKey: UnsafeRawPointer) -> Any? {
        guard let valueAsType = objc_getAssociatedObject(self, associativeKey) else {
            return nil
        }
        return valueAsType
    }
}


